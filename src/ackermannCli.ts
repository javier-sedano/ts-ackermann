import { ackermann } from "./ackermann";

function usage(): void {
  console.log(`Usage: ${process.argv[1]} <m> <n>`);
}

export function main(args: string[]): void {
  if (args.length !== 4) {
    usage();
    return;
  }

  const m: number = parseInt(args[2]);
  const n: number = parseInt(args[3]);

  if ((!Number.isInteger(m))
    || (!Number.isInteger(n))
  ) {
    usage();
    return;
  }
  const result = ackermann(m, n);
  console.log(result);
}
