import { describe, expect, it } from '@jest/globals';
import { ackermann } from './ackermann';

describe("ackermann", () => {

  it("should return 0 on non-integer m", () => {
    expect(ackermann(0.1, 1)).toEqual(0);
  });

  it("should return 0 on non-integer n", () => {
    expect(ackermann(1, 0.1)).toEqual(0);
  });

  it("should return 0 on negative m", () => {
    expect(ackermann(-1, 1)).toEqual(0);
  });

  it("should return 0 on negative n", () => {
    expect(ackermann(1, -1)).toEqual(0);
  });

  it("should return 7 on 2,2", () => {
    expect(ackermann(2, 2)).toEqual(7);
  });

});
