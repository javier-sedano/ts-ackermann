import { describe, expect, it, beforeEach, afterEach, jest } from '@jest/globals';
import * as ackermannFunctions from './ackermann';
import { main } from './ackermannCli';

describe("ackermann main", () => {
  let ackermannMock;
  beforeEach(() => {
    ackermannMock = jest.spyOn(ackermannFunctions, "ackermann").mockReturnValue(42);
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("Should exit when called without arguments", () => {
    main([
      "node",
      "cli",
    ]);
    expect(ackermannMock).toHaveBeenCalledTimes(0);
  });

  it("Should exit when called with wrong m and n", () => {
    main([
      "node",
      "cli",
      "a",
      "2",
    ]);
    expect(ackermannMock).toHaveBeenCalledTimes(0);
  });

  it("Should exit when called with m and wrong n", () => {
    main([
      "node",
      "cli",
      "2",
      "a",
    ]);
    expect(ackermannMock).toHaveBeenCalledTimes(0);
  });

  it("Should use the function when called with m and n", () => {
    main([
      "node",
      "cli",
      "1",
      "2",
    ]);
    expect(ackermannMock).toHaveBeenCalledTimes(1);
    expect(ackermannMock).toHaveBeenCalledWith(1, 2);
  });

  it("Should use the function rounding when called with float m and float n", () => {
    main([
      "node",
      "cli",
      "1.1",
      "2.2",
    ]);
    expect(ackermannMock).toHaveBeenCalledTimes(1);
    expect(ackermannMock).toHaveBeenCalledWith(1, 2);
  });
});
