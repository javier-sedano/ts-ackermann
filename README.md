# ts-ackermann

Implementation of the Ackermann-Péter function. https://en.wikipedia.org/wiki/Ackermann_function

## Usage
```
npm install ts-ackermann
yarn add ts-ackermann
npx ts-ackermann
const ackermann = require('ts-ackermann');
import { ackermann } from 'ts-ackermann';
```

## Contribution

```
yarn initDependencies
```
